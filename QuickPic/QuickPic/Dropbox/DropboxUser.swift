//
//  DropboxUser.swift
//  QuickPic
//
//  Created by Arun on 16/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import Foundation
import SwiftyDropbox
import Alamofire

protocol  DropboxUserSession{
    func userSessionValid(shouldStartSync: Bool)
    func sessionInvalid()
    func userAuthError(message: String)
}

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}

class DropboxUser {
    
    static let sharedInstance = DropboxUser()

    private var userSessionProtocol : DropboxUserSession?
    
    init(){
    }
    
    func initializeUser(observer: DropboxUserSession) {
        print("\(Date()) 🐥\(type(of: self)) \(#function)")
        self.userSessionProtocol = observer
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if(self.isUserSignedIn()){
                //Sync would have started from network monitor...
                self.userSessionProtocol?.userSessionValid(shouldStartSync: false)
            }
            else{
                self.userSessionProtocol?.sessionInvalid()
            }
        }
    }
    
    private func isUserSignedIn() -> Bool{
        let state = !(DropboxClientsManager.authorizedClient == nil)
        print("\(Date()) 🐥\(type(of: self)) \(#function) \(state ? "Yes" : "No")")
        return state
    }

    func canAttemptSync() -> Bool{
        let state = (Connectivity.isConnectedToInternet &&  self.isUserSignedIn())
        print("\(Date()) 🐥\(type(of: self)) \(#function) \(state ? "Yes" : "No")")
        return state

    }
    
    init(userProtocol:DropboxUserSession) {
        self.userSessionProtocol = userProtocol
    }
    
    func startNewSession(){
        print("\(Date()) 🐥\(type(of: self)) \(#function)")
        if(self.isUserSignedIn()){
            self.userSessionProtocol?.userSessionValid(shouldStartSync: false)
            return
        }
        print("\(Date()) 🐥\(type(of: self)) \(#function) Starting web view")
         DropboxClientsManager.authorizeFromController(UIApplication.shared, controller: UIApplication.topViewController(), openURL: {(url: URL) -> Void in
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
    }
    
    func dropSession(){
        print("\(Date()) 🐥\(type(of: self)) \(#function)")
        DropboxClientsManager.resetClients()
        self.userSessionProtocol?.sessionInvalid()
    }
    
    func handleEvent(result : DropboxOAuthResult){
        print("\(Date()) 🐥\(type(of: self)) \(#function) \(result)")
        switch result {
        case .success(_):
            self.userSessionProtocol?.userSessionValid(shouldStartSync: true)
        case .cancel:
            self.userSessionProtocol?.userAuthError(message: "Cancelled login")
        case .error(_, let description):
            self.userSessionProtocol?.userAuthError(message: description)
        }
    }
}
