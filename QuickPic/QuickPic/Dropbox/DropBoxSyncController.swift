//
//  DropBoxSyncController.swift
//  QuickPic
//
//  Created by Arun on 15/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit
import SwiftyDropbox


class DropBoxSyncController {

    private var syncUpdate: SyncProtocol? = nil
    private weak var dbService : DBHandler? = nil
    private var operationQueue : SyncQueue = SyncQueue()
    private var networkMonitor : NetworkMonitor?

    init(withDBService : DBHandler, syncProtocol: SyncProtocol) {
        self.dbService = withDBService
        self.syncUpdate = syncProtocol
        self.networkMonitor = NetworkMonitor(monitor: self)
    }
    
    //MARK:- Public Handlers
    func prepareForSync(){

        if(!DropboxUser.sharedInstance.canAttemptSync()){
            print("\(Date()) 📦 \(type(of: self)) >> 🔴 Dropbbox Sync Not possible")
            return
        }
        
        //Lets cancel all the operations and satrt again.
        self.cancelOperations()
        
        self.getDropboxFolderContents { (files, dropboxStatus) in
            if(dropboxStatus == OperationReturnStatus.OperationAuthFail){
                self.handleDropboxAuthError()
                print("\(Date()) 📦 \(type(of: self)) >> 🔴 Sync interrupted")
                return
            }
            self.dbService?.readImagesFromDB(completion: { (images) in
                self.prepareSyncList(remote: files, localDB: images)
            })
        }
    }
    
    func newFileToUpload(file:String){
        print("\(Date()) 📦 \(type(of: self)) \(file)")
        self.uploadImage(name: file)
    }
    
    func cancelOperations(){
        print("\(Date()) 📦 \(type(of: self)) >> 🔴 Dropbbox Sync Cancelling operations")
        self.operationQueue.cancelAllOperations()
    }
    
    //MARK:- Operation Handlers
    private func handleDropboxAuthError(){
        self.cancelOperations()
        DropboxUser.sharedInstance.dropSession()
    }
    
    private func prepareSyncList(remote: [DropboxImageModel]?, localDB: [PictureModel]?){
        let toUploadFiles = localDB?.filter {
            !$0.isSynced
        }
        
        var downloadList = [DropboxImageModel]()
        remote?.forEach { (remoteFile) in
            let matchingFileFound = localDB?.filter({ $0.pictureName == remoteFile.pictureName})
            if(matchingFileFound?.count == 0){
                downloadList.append(remoteFile)
            }
        }
        
        downloadList.forEach({
            self.downloadImage(imageName: $0.pictureName!)
        })
        
        toUploadFiles?.forEach({
            self.uploadImage(name: $0.pictureName!)
        })
        
        print("\(Date()) 📦 \(type(of: self)) >> Upload Files List \(String(describing: toUploadFiles)) DownloadFiles List: \(downloadList)")
        
    }
    
    private func getDropboxFolderContents(completion: @escaping ([DropboxImageModel]?, OperationReturnStatus) -> Void){
        
         let client = DropboxClientsManager.authorizedClient
            
            var imageArray : [DropboxImageModel] = [DropboxImageModel]()
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        _ = client?.files.listFolder(path: "", recursive: false).response { response, callError in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            if let result = response  {
                for entry in result.entries {
                    if entry.name.hasSuffix("." + IMAGE_EXTENSION_SUPPORTED) {
                        imageArray.append(DropboxImageModel(pictureName: entry.name, createdDate:Date()))
                    }
                }
                print("\(Date()) 📦 Dropbox Account >>> \(imageArray)")
                completion(imageArray, OperationReturnStatus.OperationSuccess)
            }
            else if let callError = callError{
                var state : OperationReturnStatus = OperationReturnStatus.OperationFailed
                switch callError as CallError {
                    
                case .internalServerError(_, _, _):
                    state = OperationReturnStatus.OperationAuthFail
                case .authError(_, _, _, _):
                    print("\(Date()) ")
                    
                default:
                    break
                }
                completion(nil, state)
            }
        }
    }
    
    private func uploadImage(name:String){
        
        if(!DropboxUser.sharedInstance.canAttemptSync()){
            print("\(Date()) 📦 \(type(of: self)) >> 🔴 Dropbbox Sync Not possible")
            return
        }
        
        self.operationQueue.addUploadOperation(imageName: name) { (status) in
            if(status == OperationReturnStatus.OperationSuccess){
                self.syncUpdate?.newFileUploaded(name: name)
            }
            else if(status == OperationReturnStatus.OperationAuthFail){
                print("\(Date()) 📦 \(type(of: self)) >> 🔴 Dropbbox Sync Auth Error, reset user")
                self.handleDropboxAuthError()
            }
        }
    }

    private func downloadImage(imageName:String){
        if(!DropboxUser.sharedInstance.canAttemptSync()){
            print("\(Date()) 📦 \(type(of: self)) >> 🔴 Dropbbox Sync Not possible")
            return
        }
        self.operationQueue.addDownloadOperation(imageName: imageName) { (data, status) in
            if(status == OperationReturnStatus.OperationSuccess){
                self.syncUpdate?.newFileDownloaded(name: imageName, data: data!)
            }
            else if(status == OperationReturnStatus.OperationAuthFail){
                print("\(Date()) 📦 \(type(of: self)) >> 🔴 Dropbbox Sync Auth Error, reset user")
                self.handleDropboxAuthError()
            }
        }
    }
}

//MARK:- NetworkProtocol
extension DropBoxSyncController : NetworkProtocol{
    func networkChanged() {
        print("\(Date()) 🛎🌐 \(type(of: self))")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if !Connectivity.sharedInstance.isReachable{
                self.cancelOperations()
            }
            else{
                print("\(Date()) 📦 \(type(of: self)) >> Preparing for Sync")
                self.prepareForSync()
            }
        }
    }
}

