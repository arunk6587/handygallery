//
//  SyncQueue.swift
//  QuickPic
//
//  Created by Arun on 15/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import Foundation
import SwiftyDropbox

enum OperationReturnStatus{
    case OperationSuccess
    case OperationFailed
    case OperationAuthFail
}

class SyncQueue: NSObject {

    private var syncOperationsQueue : OperationQueue?

    override init() {
        self.syncOperationsQueue = OperationQueue.init()
        self.syncOperationsQueue?.maxConcurrentOperationCount = 1;
    }
    
    func resumeOperations(){
        self.syncOperationsQueue?.isSuspended = false
    }
    
    func suspendOperations(){
        self.syncOperationsQueue?.isSuspended = true
    }
    
    func cancelAllOperations(){
        self.syncOperationsQueue?.cancelAllOperations()
    }
    
    
    internal func addDownloadOperation(imageName:String, processCompletion: ((Data?, OperationReturnStatus) -> Void)?)
    {
        self.syncOperationsQueue?.addOperation({
            let client = DropboxClientsManager.authorizedClient
            print("\(Date()) 🤹‍♂️ \(type(of: self)) >> \(#function) >> \(imageName)")
            let filePath = "/" + imageName
            let resposne = client?.files.download(path: filePath)
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            resposne?.response(completionHandler: { (result, callError) in
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                if let result = result{
                    let data = result.1
                    if(data.count > 0){
                        print("\(Date()) 🤹‍♂️ \(type(of: self)) >> Downloaded \(imageName)")
                        processCompletion!(result.1, OperationReturnStatus.OperationSuccess)
                    }
                    else{
                        processCompletion!(nil, OperationReturnStatus.OperationFailed)
                    }
                }
                else if let callError = callError{
                    var state : OperationReturnStatus = OperationReturnStatus.OperationFailed
                    switch callError as CallError {

                    case .internalServerError(_, _, _):
                        state = OperationReturnStatus.OperationAuthFail
                    case .authError(_, _, _, _):
                        print("\(Date()) ")

                    default:
                        break
                    }
                    processCompletion!(nil, state)
                }
            })
        })
    }
    
    internal func addUploadOperation(imageName:String, processCompletion: ((OperationReturnStatus) -> Void)?)
    {
        self.syncOperationsQueue?.addOperation({
            let client = DropboxClientsManager.authorizedClient
            print("\(Date()) 🤹‍♂️ \(type(of: self)) >> \(#function) >> \(imageName)")
            let filePath = "/" + imageName

            FolderManager.sharedInstance.getPictureURL(imageName: imageName, completion: { (status, url) in
                if(status){
                    var imageData : Data? = nil
                    do {
                        imageData = try Data.init(contentsOf: url!)
                    }
                    catch {
                        processCompletion!(OperationReturnStatus.OperationFailed)
                        return
                    }
                    
                    if let imageDataProcessed = imageData{
                        let resposne = client?.files.upload(path: filePath, mode: .add, autorename: false, clientModified: Date(), mute: false, propertyGroups: nil, input: imageDataProcessed)
                        DispatchQueue.main.async {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = true
                        }
                        resposne?.response(completionHandler: { (metaData, callError) in
                            DispatchQueue.main.async {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                            if metaData != nil{
                                print("\(Date()) 🤹‍♂️ \(type(of: self)) >> Uploaded \(imageName)")
                                processCompletion!(OperationReturnStatus.OperationSuccess)
                            }
                            else if let callError = callError{
                                var state : OperationReturnStatus = OperationReturnStatus.OperationFailed
                                switch callError as CallError {
                                    
                                case .internalServerError(_, _, _):
                                    state = OperationReturnStatus.OperationAuthFail
                                case .authError(_, _, _, _):
                                    print("\(Date()) ")
                                    
                                default:
                                    break
                                }
                                processCompletion!(state)
                            }
                        })
                    }
                    else{
                        processCompletion!(OperationReturnStatus.OperationFailed)
                    }
                }
                else{
                    processCompletion!(OperationReturnStatus.OperationFailed)
                }
            })
        })
    }
}
