//
//  Constants.swift
//  QuickPic
//
//  Created by Arun on 10/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

let SEGUE_IDENDIFIER_DIRECTORY_VC   = "RevealFolder"
let SEGUE_IDENDIFIER_FOLDER_LIST_VC = "FolderList"

let SEGUE_IDENDIFIER_COMMENTVIEWER_VC   = "CommentViewer"

let IMAGE_EXTENSION_SUPPORTED = "png"
let IMAGE_NAME_FORMAT = "yyyy-MM-dd-'T'-HH-mm-ss-SSS"

let DB_PIC_ID_KEY = "name"
