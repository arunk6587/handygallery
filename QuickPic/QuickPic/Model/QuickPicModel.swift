//
//  QuickPicModel.swift
//  QuickPic
//
//  Created by Arun on 9/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit
import CoreData

enum ModelType{
    case Picture_Type
    case Folder_Type
}

class BaseModel: NSObject {
    public private(set) var modelType : ModelType = .Folder_Type
    public private(set) var fileURL : URL? = nil
    public private(set) var createdDate : Date? = nil
    init(model : ModelType, createdDate:Date, fileURL:URL) {
        self.modelType = model
        self.createdDate = createdDate
        self.fileURL = fileURL
    }
    
}

class FolderModel: BaseModel {
    public private(set) var folderName : String?
    public private(set) var picturesCount : Int?

    init(foldername: String, createdDate: Date, picturesCount: Int, fileURL: URL){
        super.init(model:.Folder_Type, createdDate: createdDate, fileURL: fileURL)
        self.folderName = foldername
        self.picturesCount = picturesCount
    }
    
    func cutImageToNewFolder(folder: FolderModel, image: PictureModel, directoryService:FolderManager) -> Bool{
        let status = folder.moveImage(image: image, directoryService: directoryService)
        if(status){
            self.picturesCount! -= 1
        }
        return status
    }
    
    func moveImage(image: PictureModel, directoryService:FolderManager) -> Bool{
       let status = directoryService.moveImage(fromPath:image.fileURL!, toFolder: self.folderName!)
        if(status){
            self.picturesCount! += 1
        }
        
        return status
    }
    
    func readPicturesFromFolder(directoryService:FolderManager, completion: @escaping (([PictureModel]?, Bool) -> Void)){
        
        directoryService.readPicturesFromFolder(folderName: folderName!) { (pictures, status) in
            if(status){
                self.picturesCount = pictures!.count
            }
            completion(pictures, status)
        }
    }
}

class PictureModel: BaseModel {
    public private(set) weak var folder : FolderModel? = nil
    public private(set) var pictureName : String?
    public private(set) var image : UIImage?
    public private(set) var isSynced : Bool = false

    static func getDummyObject(name:String) -> PictureModel{
        print("\(Date()) Dummy \(name)")
        return PictureModel(pictureName: name, createdDate: Date(), url: URL(fileURLWithPath: "\\"), synced: false)
    }
    
    init(pictureName:String, createdDate: Date, url: URL, synced: Bool){
        super.init(model:.Picture_Type, createdDate: createdDate, fileURL: url)
        self.pictureName = pictureName
        self.isSynced = synced
        //self.description(reference: "Creating")
    }
    
    func getImage(completion: @escaping (_ image: UIImage) -> Void)
    {
        //print("\(Date()) 🏙\(type(of: self)) \(#function) \(self.pictureName ?? "Name 🔴")")
        self.getThumbNail(fileURL: self.fileURL!,completion: completion)
    }
    
    private func getThumbNail(fileURL: URL,completion: @escaping (_ image: UIImage) -> Void) {
        //print("\(Date()) 🏙\(type(of: self)) \(#function) \(self.pictureName ?? "Name 🔴")")
        if((self.image) != nil){
            completion(self.image!)
            return
        }
        DispatchQueue.global(qos: .userInteractive).async {
            let options = [
                kCGImageSourceCreateThumbnailWithTransform: false,
                kCGImageSourceCreateThumbnailFromImageAlways: true,
                kCGImageSourceThumbnailMaxPixelSize: 150] as CFDictionary
            let imageSource = CGImageSourceCreateWithURL(fileURL as CFURL, nil)
            let imageReference = CGImageSourceCreateThumbnailAtIndex(imageSource!, 0, options)!
            let image = UIImage(cgImage: imageReference)
            self.image = image
            completion(image)
        }
    }
    
    func addToDB(completion: @escaping (Error?) -> Void){
        print("\(Date()) 🏙\(type(of: self)) \(#function) \(self.pictureName ?? "Name 🔴")")
        FolderManager.sharedInstance.dbHandler?.addImage(image: self, completion: completion)
    }
    
    //MARK: DB Updates
    func switchSyncState(completion: @escaping (_ state: Error?) -> Void){
        print("\(Date()) 🏙\(type(of: self)) \(#function) \(self.pictureName ?? "Name 🔴")")
        FolderManager.sharedInstance.dbHandler?.switchSyncStateOfImage(image: self.pictureName!, completion: { (error) in
            if(error == nil){
                self.isSynced = !self.isSynced
            }
            completion(error)
        })
    }
    
    func getSyncState(){
        //print("\(Date()) 🏙\(type(of: self)) \(#function) \(self.pictureName ?? "Name 🔴")")
        FolderManager.sharedInstance.dbHandler?.getSyncStateOfImage(image: self.pictureName!, completion: { (status) in
            self.isSynced = status
        })
    }
    
    func getcomment(completion: @escaping (_ comment: String?) -> Void){
        print("\(Date()) 🏙\(type(of: self)) \(#function) \(self.pictureName ?? "Name 🔴")")
        FolderManager.sharedInstance.dbHandler?.getComment(image: self.pictureName!, completion: completion)
    }
    
    func updateComment(image:String, comment:String, completion: @escaping (Error?) -> Void)
    {
        print("\(Date()) 🏙\(type(of: self)) \(#function) \(self.pictureName ?? "Name 🔴") [\(comment)]")
        FolderManager.sharedInstance.dbHandler?.updateComment(image: image, comment: comment, completion: completion)
    }
    
    func description(reference:String){
        print("\(Date()) 🏙 \(type(of: self)) >> \(reference) [\(pictureName ?? "🔴")] [\(createdDate?.description ?? "0️⃣")] [\(isSynced ? "True" : "False")]")
    }
}



extension PictureModel: ManagedObjectConvertible {
    func toManagedObject(in context: NSManagedObjectContext) -> Image? {
        //self.description(reference: "Create/Fetch Entity")
        
        let imageDB = Image.getOrCreateSingle(with: pictureName!, from: context)
        imageDB.isSynced = isSynced
        imageDB.createdDate = createdDate
        
        //print("\(Date()) 🏙 \(type(of: self)) >> Entity Info created : [\(imageDB.name ?? "🔴")] [\(imageDB.createdDate?.description ?? "0️⃣")] [\(imageDB.isSynced ? "True" : "False")]")
        return imageDB
    }
}


class DropboxImageModel {
    public private(set) var pictureName : String?
    public private(set) var createdDate : Date?
    
    init(pictureName: String, createdDate: Date){
        self.pictureName = pictureName
        self.createdDate = createdDate
    }
}
