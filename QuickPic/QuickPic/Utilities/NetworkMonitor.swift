//
//  NetworkMonitor.swift
//  QuickPic
//
//  Created by Arun on 16/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import Foundation
import Reachability

protocol NetworkProtocol{
    func networkChanged()
}

class NetworkMonitor: NSObject {
    var reachability: Reachability!

    var networkObserver : NetworkProtocol?
    
    init(monitor : NetworkProtocol) {
        super.init()
        self.networkObserver = monitor
        reachability = Reachability()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do{
            try reachability.startNotifier()
        }catch{
            print("\(Date()) 🌐 \(type(of: self)) >> 🔴 Failed to start")
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        print("\(Date()) 🌐 \(type(of: self)) >> Changed")

        self.networkObserver?.networkChanged()
    }
}


