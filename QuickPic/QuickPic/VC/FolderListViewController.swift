//
//  FolderListViewController.swift
//  QuickPic
//
//  Created by Arun on 10/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

class FolderListViewController: BaseViewController {

    private var userPresenter : FolderListPresenter? = nil
    var dismissCompletion: DismissCompletion?
    private var folderList : [FolderModel] = [FolderModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.userPresenter?.attachView(view: self)
        self.userPresenter?.readFolders()
        
        self.imageFolderCollectionView.register(UINib(nibName: FolderCell.identifier, bundle: nil), forCellWithReuseIdentifier: FolderCell.cellIdentifier)
        self.title = "Moving images to"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelAction))

        // Do any additional setup after loading the view.
    }

    @objc func cancelAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setFolderAndCompletion(selectedFolder:FolderModel, tapCompletion: @escaping DismissCompletion){
        
        self.userPresenter = FolderListPresenter(currentFolder:selectedFolder)
        self.dismissCompletion = tapCompletion
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FolderListViewController : FolderListView{
    func foldersUpdated(folders: [FolderModel]?) {
        self.folderList.removeAll()
        if(folders != nil){
            self.folderList.append(contentsOf: folders!)
        }
            self.imageFolderCollectionView.reloadData()
    }
}



//MARK:- UICollectionViewDataSource Protocols
extension FolderListViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.folderList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let folderData = self.folderList[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FolderCell.cellIdentifier, for: indexPath) as! FolderCell
            cell.setDetail(folder: folderData)
            return cell
        }

}

//MARK:- UICollectionViewDelegateFlowLayout Protocols
extension FolderListViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize              = UIScreen.main.bounds
        let screenWidth             = screenSize.width
        let cellSquareSize: CGFloat = (screenWidth / CGFloat(numberOfRows)) - 10
        
        return CGSize.init(width: cellSquareSize, height: heightOfCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
}

//MARK:- UICollectionViewDelegate Protocols
extension FolderListViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dismissCompletion!(self.folderList[indexPath.row])
        self.folderList.removeAll()
        dismissCompletion = nil
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool{
        
        return true
    }
}

