//
//  BaseViewController.swift
//  QuickPic
//
//  Created by Arun on 10/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

let numberOfRows : Int = 3
let heightOfCell : CGFloat = 100.0

class BaseViewController: UIViewController {
    @IBOutlet weak var imageFolderCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createBarButton(image: UIImage, action:Selector) -> UIBarButtonItem{
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.frame = CGRect(x: 0.0, y: 0.0, width: 35.0, height: 35.0)
        button.addTarget(self, action: action, for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


