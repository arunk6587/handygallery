//
//  DirectoryViewController.swift
//  QuickPic
//
//  Created by Arun on 9/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit
import ImageViewer

typealias DismissCompletion = ((_ selectedFolder : FolderModel) -> Void)

class DirectoryViewController: BaseViewController {

    @IBOutlet weak var statusLabel: UILabel!
    
    private var selectedImageForCommentViewing : String? = ""
    
    private var viewPresenter : DirectoryViewPresenter? = nil
    var picsReArrangedCompletion: PicsReArrangedCompletion?
    private var picturesArray : [PictureModel] = [PictureModel]()
    weak var folder : FolderModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imageFolderCollectionView.register(UINib(nibName: PictureCell.identifier, bundle: nil), forCellWithReuseIdentifier: PictureCell.cellIdentifier)
        viewPresenter = DirectoryViewPresenter(folderInAction: self.folder!)
        self.viewPresenter?.attachView(view: self)
        self.statusLabel.text = "Fething Images."
        self.imageFolderCollectionView.isHidden = true
        self.viewPresenter?.loadFolderContents()
        self.title = folder?.folderName
        
        self.updateUIForReArrangeMode(onRearrangeMode: (self.viewPresenter?.queryRearrangeModeState())!)
        // Do any additional setup after loading the view.
    }

    func setCompletion (rearrangeCompletion:@escaping PicsReArrangedCompletion){
        self.picsReArrangedCompletion = rearrangeCompletion
    }
    
    private func updateUIForReArrangeMode(onRearrangeMode:Bool){
        if(onRearrangeMode){
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(reArrrangeAction))
            
            self.navigationItem.leftBarButtonItem = self.createBarButton(image: UIImage (named: "Folder_Add")!, action: #selector(backAction))

            self.navigationItem.leftBarButtonItem?.isEnabled = false
        }
        else{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Select", style: UIBarButtonItemStyle.plain, target: self, action: #selector(reArrrangeAction))
            self.navigationItem.leftBarButtonItem = self.createBarButton(image: UIImage (named: "Back")!, action: #selector(backAction))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @objc private func backAction(){
        if(!(self.viewPresenter?.queryRearrangeModeState())!){
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.performSegue(withIdentifier: SEGUE_IDENDIFIER_FOLDER_LIST_VC, sender: self)
        }
    }

    @objc private func reArrrangeAction(){
        self.viewPresenter?.switchReArrangeMode()
    }
    
    @objc private func commentButtonAction(sender: CustomButton?){
        self.viewPresenter?.commentButtonTapped(fileName: (sender?.fileID)!)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SEGUE_IDENDIFIER_FOLDER_LIST_VC == segue.identifier {
            let destinationNavigationController = segue.destination as! UINavigationController
            let targetController = destinationNavigationController.topViewController as! FolderListViewController
            
            targetController.setFolderAndCompletion(selectedFolder: self.folder!) { (folderSeleted) in
                targetController.dismiss(animated: true, completion: {
                    self.viewPresenter?.moveToFolder(folder: folderSeleted)
                })
            }
        }
        else if SEGUE_IDENDIFIER_COMMENTVIEWER_VC == segue.identifier{
            let destinationNavigationController = segue.destination as! UINavigationController
            let targetController = destinationNavigationController.topViewController as! CommentViewerViewController
            targetController.imagename = self.selectedImageForCommentViewing
        }
    }
    
    // MARK: - Image Viewer Config
    func galleryConfiguration() -> GalleryConfiguration {
        
        return [
            GalleryConfigurationItem.deleteButtonMode(.none),
            GalleryConfigurationItem.seeAllCloseButtonMode(.none),
            GalleryConfigurationItem.closeButtonMode(.builtIn)
        ]
    }
 }

//MARK:- GalleryItemsDataSource Protocols
extension DirectoryViewController : GalleryItemsDataSource{
    func itemCount() -> Int {
        return self.picturesArray.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        
        let image = (UIImage(contentsOfFile: (self.picturesArray[index].fileURL?.path)!))
        
        return GalleryItem.image { $0(image) }
    }
    
    
}

//MARK:- DashboardPresenter Protocols
extension DirectoryViewController: DirectoryView{
    func directoryDetailsRefreshed(folderDetails: [PictureModel]?) {
        self.picturesArray.removeAll()
        if(folderDetails != nil){
            self.picturesArray.append(contentsOf: folderDetails!)
            self.imageFolderCollectionView.isHidden = false
            self.imageFolderCollectionView.reloadData()
        }
        else{
            self.statusLabel.text = "No Images to show."
            self.imageFolderCollectionView.isHidden = true
        }
    }
    
    func picturesRemoved(atIndexes: [Int], newModeList: [PictureModel]) {
        self.imageFolderCollectionView.performBatchUpdates({
            self.picturesArray.removeAll()
            self.picturesArray.append(contentsOf: newModeList)
            var indexPaths : [IndexPath] = [IndexPath]()
            atIndexes.forEach({ (index) in
                indexPaths.append(IndexPath(item: index, section: 0))
            })
            self.imageFolderCollectionView.deleteItems(at: indexPaths)
        }) { (completed) in
            if(self.picturesArray.count == 0){
                self.statusLabel.text = "No more images in this folder."
                self.imageFolderCollectionView.isHidden = true
            }
        }
        
        self.picsReArrangedCompletion!(true)
    }
    
    
    func updateSelectionOnUserTap(index: Int, isAdded: Bool, canEnableMove: Bool) {
        let cell = self.imageFolderCollectionView.cellForItem(at: IndexPath(item: index, section: 0))
        isAdded ? (cell?.contentView.backgroundColor = UIColor.blue) : (cell?.contentView.backgroundColor = UIColor.lightText)
        self.navigationItem.leftBarButtonItem?.isEnabled = canEnableMove
    }
    
    func updateRearrangeMode(onReArrangeMode: Bool, refreshIndexes: [Int]) {
        self.updateUIForReArrangeMode(onRearrangeMode: onReArrangeMode)
        
        if(refreshIndexes.count > 0){
            var indexesToReload : [IndexPath] = [IndexPath]()
            refreshIndexes.forEach {
                indexesToReload.append(IndexPath(item: $0, section: 0))
            }
            self.imageFolderCollectionView.reloadItems(at: indexesToReload)
        }
    }
    
    func showCommentView(name: String) {
        self.selectedImageForCommentViewing = name
        self.performSegue(withIdentifier: SEGUE_IDENDIFIER_COMMENTVIEWER_VC, sender: self)
    }
}

//MARK:- UICollectionViewDataSource Protocols
extension DirectoryViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.picturesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let imageData = self.picturesArray[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PictureCell.cellIdentifier, for: indexPath) as! PictureCell
        cell.setSelection(viewPresenter: self.viewPresenter!, index:indexPath.row)
        cell.setDetail(folder: imageData)
        cell.commentBtn.addTarget(self, action: #selector(self.commentButtonAction(sender:)), for: .touchUpInside)

        return cell
    }
}

//MARK:- UICollectionViewDelegateFlowLayout Protocols
extension DirectoryViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize              = UIScreen.main.bounds
        let screenWidth             = screenSize.width
        let cellSquareSize: CGFloat = (screenWidth / CGFloat(numberOfRows)) - 10
        
        return CGSize.init(width: cellSquareSize, height: heightOfCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
}

//MARK:- UICollectionViewDelegate Protocols
extension DirectoryViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.viewPresenter?.queryRearrangeModeState())!{
            self.viewPresenter?.updateSelectionOnUserTap(pictureIndex: indexPath.row)
        }
        else{
            self.presentImageGallery(GalleryViewController(startIndex: indexPath.row, itemsDataSource: self, itemsDelegate: nil, displacedViewsDataSource: nil, configuration: self.galleryConfiguration()))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        return true
    }
}



