//
//  PictureCell.swift
//  QuickPic
//
//  Created by Arun on 9/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

class PictureCell: UICollectionViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var syncState: UIImageView!
    @IBOutlet weak var commentBtn: CustomButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var identifier: String {
        return String(describing: self)
    }
    
    static var cellIdentifier: String {
        return String(describing: self)
    }
    
    func setDetail(folder: PictureModel){
        self.commentBtn.fileID = folder.pictureName
        self.picture.image = UIImage(named: "Img_Placeholder")
        folder.getImage{ (image) in
            DispatchQueue.main.async {
                self.picture.image = image
            }
        }
        
        folder.isSynced ? (self.syncState.image = UIImage(named: "sync_ok")) : (self.syncState.image = UIImage(named: "sync_pending"))
    }
    
    func setSelection(viewPresenter : DirectoryViewPresenter, index:Int){
        
        self.contentView.backgroundColor = UIColor.lightText
        if(viewPresenter.getSelectionStatusOfImage(atIndex: index)){
            self.contentView.backgroundColor = UIColor.blue
        }
    }
}
