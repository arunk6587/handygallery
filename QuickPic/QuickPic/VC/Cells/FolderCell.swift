//
//  DashBoardFolderCell.swift
//  QuickPic
//
//  Created by Arun on 9/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

class FolderCell: UICollectionViewCell {

    @IBOutlet weak var folderNameLbl: UILabel!
    @IBOutlet weak var imageCountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var cellIdentifier: String {
        return String(describing: self)
    }
    
    func setDetail(folder: FolderModel){
        self.folderNameLbl.text = folder.folderName
        let imageCount : Int = folder.picturesCount!
        var string = ""
        switch imageCount {
        case 0:
            string = "No Images"
        case 1:
            string = "1 image"
        default:
            string = "\(imageCount) images"
        }
        self.imageCountLbl.text = string
    }
}
