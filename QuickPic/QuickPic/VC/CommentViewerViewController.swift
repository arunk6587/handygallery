//
//  CommentViewerViewController.swift
//  QuickPic
//
//  Created by Arun on 16/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

class CommentViewerViewController: UIViewController {

    @IBOutlet weak var commentTextField: UITextView!
    @IBOutlet weak var imageView: UIImageView!

    private let viewPresenter = CommentViewerPresenter()
    
    var imagename : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(imagename == nil || imagename?.count == 0){
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.viewPresenter.pictureName = imagename
        self.viewPresenter.attachView(view: self)
        self.viewPresenter.getCommentForImage()
        self.viewPresenter.getPictureURL()

        self.title = "Add Comment"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: UIBarButtonItemStyle.plain, target: self, action: #selector(updateAcion))


        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        commentTextField.becomeFirstResponder()
    }
    
    @objc func cancelAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func updateAcion(){
        
        if(commentTextField.text.count > 0){
            self.viewPresenter.updateCommentforImage(withComment: commentTextField.text)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CommentViewerViewController : CommentView{
    func commentReceived(string: String) {
        self.commentTextField.text = string
    }
    
    func commentUpdated(string: String) {
        self.commentTextField.text = string
        self.dismiss(animated: true, completion: nil)
    }
    
    func commentUpdateFailed() {
        self.commentTextField.text = ""

    }
    
    func imageURL(url: URL) {
        self.imageView.image = UIImage(contentsOfFile:(url.path))
    }
    
    
}
