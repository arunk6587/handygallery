//
//  DashboardViewController.swift
//  QuickPic
//
//  Created by Arun on 9/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit
import AudioToolbox
import ImageViewer

let LOGIN_BTN_TAG = 0
let LOGOUT_BTN_TAG = 1

typealias PicsReArrangedCompletion = ((_ shouldRefreshFolderCount : Bool) -> Void)

class DashboardViewController: BaseViewController, UINavigationControllerDelegate {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let viewPresenter = DashboardPresenter()
    private var selectedFolder : Int = 0
    private var selectedImageForCommentViewing : String = ""

    private var directoryContents : [BaseModel] = [BaseModel]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageFolderCollectionView.register(UINib(nibName: FolderCell.identifier, bundle: nil), forCellWithReuseIdentifier: FolderCell.cellIdentifier)
        self.imageFolderCollectionView.register(UINib(nibName: PictureCell.identifier, bundle: nil), forCellWithReuseIdentifier: PictureCell.cellIdentifier)
        
        self.imageFolderCollectionView.dragInteractionEnabled = true
        self.imageFolderCollectionView.dragDelegate = self
        self.imageFolderCollectionView.dropDelegate = self
        self.imageFolderCollectionView.reorderingCadence = .slow
        
        self.addNavigationBarButtons()
        
        self.viewPresenter.attachView(view: self)
        self.viewPresenter.loadAllFilesAndFolders()
        loginButton.isHidden = true

        self.activityIndicator.stopAnimating()
        // Do any additional setup after loading the view, typically from a nib.
    }

    private func addNavigationBarButtons(){
        self.navigationItem.leftBarButtonItem = self.createBarButton(image: UIImage (named: "Folder_Add")!, action: #selector(addFolderAction))
        self.navigationItem.rightBarButtonItem = self.createBarButton(image: UIImage (named: "Camera")!, action: #selector(takePictureAction))
    }
    
    private func showInputtextFieldAlertForNewFolder(){
        let alertController = UIAlertController(title: "Add Folder", message: "Enter the folder name.", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addTextField { (textField : UITextField) -> Void in
            textField.isSecureTextEntry = false
            textField.autocapitalizationType = UITextAutocapitalizationType.none
            textField.autocorrectionType = UITextAutocorrectionType.no
            textField.placeholder = "New Folder"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            
        }
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            if let textEntered = alertController.textFields?.first?.text{
                let trimmedString = textEntered.trimmingCharacters(in: .whitespaces)
                if(trimmedString.count == 0 || !self.viewPresenter.addNewFolder(name: textEntered)){
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    self.showInputtextFieldAlertForNewFolder()
                }
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showCameraNotAvailableAlert(){
        let alertController = UIAlertController(title: "Camera", message: "This device does not support camera.", preferredStyle: UIAlertControllerStyle.alert)

        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }

    private func showAnAnimationAtDrop(cellIndexPath:IndexPath){
        if let cell = self.imageFolderCollectionView.cellForItem(at: cellIndexPath){
            cell.contentView.backgroundColor = UIColor.blue
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                cell.contentView.backgroundColor = UIColor.clear
            }
        }
    }
    


    //MARK:- Actions
    @IBAction func loginAction(_ sender: Any) {
        if(loginButton.tag == LOGIN_BTN_TAG){
            self.viewPresenter.loginUIAction()
        }
        else if(loginButton.tag == LOGOUT_BTN_TAG){
            self.viewPresenter.loginOutAction()
        }
    }
    
    @objc private func addFolderAction(){
        self.showInputtextFieldAlertForNewFolder()
    }
    
    @objc private func commentButtonAction(sender: CustomButton?){
        self.viewPresenter.commentButtonTapped(fileName: (sender?.fileID)!)        
    }
    
    @objc private func takePictureAction(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            self.showCameraNotAvailableAlert()
        }
//        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = .photoLibrary;
//            imagePicker.allowsEditing = false
//            self.present(imagePicker, animated: true, completion: nil)
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if SEGUE_IDENDIFIER_DIRECTORY_VC == segue.identifier {
            let controller = segue.destination as! DirectoryViewController
            controller.folder = self.directoryContents[self.selectedFolder] as? FolderModel
            controller.setCompletion { (status) in
                if(status){
                    self.imageFolderCollectionView.reloadData()
                }
            }
        }
        else if SEGUE_IDENDIFIER_COMMENTVIEWER_VC == segue.identifier{
            let destinationNavigationController = segue.destination as! UINavigationController
            let targetController = destinationNavigationController.topViewController as! CommentViewerViewController
            targetController.imagename = self.selectedImageForCommentViewing
        }
    }
}


//MARK:- DashboardPresenter Protocols
extension DashboardViewController: DashboardView{
    
    func showCommentView(name: String) {
        self.selectedImageForCommentViewing = name
        self.performSegue(withIdentifier: SEGUE_IDENDIFIER_COMMENTVIEWER_VC, sender: self)
    }
    
    func loginUI(showLoginUI: Bool) {
        loginButton.isHidden = false
        if(showLoginUI){
            loginButton.tag = LOGIN_BTN_TAG
            loginButton.setTitle("LogIn", for: .normal)
        }
        else{
            loginButton.tag = LOGOUT_BTN_TAG
            loginButton.setTitle("LogOut", for: .normal)
        }
    }
    
    func reloadIndex(index: Int) {
        self.imageFolderCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
    }
    
    func directoryDetailsRefreshed(folderDetails: [BaseModel]?) {
        self.directoryContents.removeAll()
        if(folderDetails != nil && (folderDetails?.count)! > 0){
            self.directoryContents.append(contentsOf: folderDetails!)
            self.statusLabel.isHidden = true
        }
        else{
            self.statusLabel.isHidden = false
        }
        
        self.imageFolderCollectionView.reloadData()
    }
    
    func newItemAdded(details: BaseModel) {
        self.activityIndicator.stopAnimating()
        self.statusLabel.isHidden = true
        self.directoryContents.insert(details, at: 0)
        self.imageFolderCollectionView.insertItems(at: [IndexPath(item: 0, section: 0)])
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.imageFolderCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
        }
    }
    
    func dragAndDropUpdate(details: [BaseModel], movedItems: [Int], picturesMovedToIndex: Int) {
        self.directoryContents.removeAll()
        self.directoryContents.append(contentsOf: details)
        self.imageFolderCollectionView.performBatchUpdates({
            var indexArray = [IndexPath]()
            movedItems.forEach({
                indexArray.append(IndexPath(item: $0, section: 0))
            })
            self.imageFolderCollectionView.deleteItems(at: indexArray)
        }) { (completed) in
            let dropIndex = IndexPath(item: picturesMovedToIndex, section: 0)
            self.imageFolderCollectionView.reloadItems(at: [dropIndex])
            self.showAnAnimationAtDrop(cellIndexPath: dropIndex)
        }
    }
}



//MARK:- UICollectionViewDataSource Protocols
extension DashboardViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.directoryContents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let data = self.directoryContents[indexPath.row]
        if(data.modelType == .Folder_Type){
            let folderData = data as! FolderModel
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FolderCell.cellIdentifier, for: indexPath) as! FolderCell
            cell.setDetail(folder: folderData)
            return cell
        }
        else{
            let imageData = data as! PictureModel
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PictureCell.cellIdentifier, for: indexPath) as! PictureCell
            cell.commentBtn.tag = indexPath.row
            cell.commentBtn.addTarget(self, action: #selector(self.commentButtonAction(sender:)), for: .touchUpInside)
            cell.setDetail(folder: imageData)
            return cell
        }
    }
}

//MARK:- UICollectionViewDelegate Protocols
extension DashboardViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let object = self.directoryContents[indexPath.row]
        if(object.modelType == .Folder_Type){
            if(object as! FolderModel).picturesCount! > 0{
                self.selectedFolder = indexPath.row
                self.performSegue(withIdentifier: SEGUE_IDENDIFIER_DIRECTORY_VC, sender: self)
            }
        }
        else{
            
            let imagesArray = self.directoryContents.filter{
                $0.modelType == .Picture_Type
            }
            let indexOf = imagesArray.index(of: object)
            self.presentImageGallery(GalleryViewController(startIndex: indexOf!, itemsDataSource: self, itemsDelegate: nil, displacedViewsDataSource: nil, configuration: self.galleryConfiguration()))
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }

    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {

        return true
    }
    
    func galleryConfiguration() -> GalleryConfiguration {
        
        return [
            GalleryConfigurationItem.deleteButtonMode(.none),
            GalleryConfigurationItem.seeAllCloseButtonMode(.none),
            GalleryConfigurationItem.closeButtonMode(.builtIn),
            GalleryConfigurationItem.spinnerStyle(.whiteLarge)
        ]
    }
}

//MARK:- GalleryItemsDataSource Protocols
extension DashboardViewController : GalleryItemsDataSource{
    func itemCount() -> Int {
        return self.directoryContents.filter{
            $0.modelType == .Picture_Type
        }.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        let imagesArray = self.directoryContents.filter{
            $0.modelType == .Picture_Type
            }
        
        let image = (UIImage(contentsOfFile: (imagesArray[index].fileURL?.path)!))
        
        return GalleryItem.image { $0(image) }
    }
    
    
}

//MARK:- UICollectionViewDelegateFlowLayout Protocols
extension DashboardViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize              = UIScreen.main.bounds
        let screenWidth             = screenSize.width
        let cellSquareSize: CGFloat = (screenWidth / CGFloat(numberOfRows)) - 10
        
        return CGSize.init(width: cellSquareSize, height: heightOfCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
}


//MARK:- UIImagePickerControllerDelegate Protocols
extension DashboardViewController: UIImagePickerControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let imgTemp = info[UIImagePickerControllerOriginalImage] as? UIImage// get image from imagePickerViewController
        
        picker.dismiss(animated: true) {
            self.activityIndicator.startAnimating()
           _ = self.viewPresenter.saveNewImage(image: imgTemp!.fixOrientation())

        }
    }
    
}

//MARK:- UICollectionViewDragDelegate Protocols
extension DashboardViewController : UICollectionViewDragDelegate{
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let model = self.directoryContents[indexPath.item]
        if model.modelType == .Picture_Type{
            let pictureModel = model as! PictureModel
            let itemProvider = NSItemProvider(object: pictureModel.fileURL! as NSURL)
            let dragItem = UIDragItem(itemProvider: itemProvider)
            dragItem.localObject = pictureModel
            return [dragItem]
        }
        else{
            return [UIDragItem]()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem]
    {
        let model = self.directoryContents[indexPath.item]
        if model.modelType == .Picture_Type{
            let pictureModel = model as! PictureModel
            let itemProvider = NSItemProvider(object: pictureModel.fileURL! as NSURL)
            let dragItem = UIDragItem(itemProvider: itemProvider)
            dragItem.localObject = pictureModel
            return [dragItem]
        }
        else{
            return [UIDragItem]()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dragSessionWillBegin session: UIDragSession) {
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
    }
    
    func collectionView(_ collectionView: UICollectionView, dragSessionDidEnd session: UIDragSession) {
        
    }
}

//MARK:- UICollectionViewDropDelegate Protocols
extension DashboardViewController : UICollectionViewDropDelegate{
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        if let destinationIndexPath = coordinator.destinationIndexPath{

            var sourceIndexesArray : [Int] = [Int]()

            coordinator.items.forEach({
                sourceIndexesArray.append(($0.sourceIndexPath?.row)!)
            })
            
            self.viewPresenter.sortImagesAfterDragNDrop(sourceImagesIndexes: sourceIndexesArray, toDestinationFolderIndex: destinationIndexPath.row)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSURL.self)
    }
    

    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal{
        if let dropIndex = destinationIndexPath{
            let model = self.directoryContents[(dropIndex.row)]
            if model.modelType == .Folder_Type{
                return UICollectionViewDropProposal(operation: .move, intent: .insertIntoDestinationIndexPath)
            }
            else{
                return UICollectionViewDropProposal(operation: .forbidden)
            }
        }
        else{
            return UICollectionViewDropProposal(operation: .forbidden)
        }
    }
}


