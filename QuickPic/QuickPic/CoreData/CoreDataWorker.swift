//
//  CoreDataWorker.swift
//  DBTest
//
//  Created by Arun on 15/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import CoreData

protocol CoreDataWorkerProtocol {
    associatedtype EntityType
    associatedtype Entity
    func get(with predicate: NSPredicate?,
             sortDescriptors: [NSSortDescriptor]?,
             fetchLimit: Int?,
             completion: @escaping (Result<[EntityType]>) -> Void)
    func upsert(entity: EntityType,
                completion: @escaping (Error?) -> Void)
}

extension CoreDataWorkerProtocol {
    func get(with predicate: NSPredicate? = nil,
             sortDescriptors: [NSSortDescriptor]? = nil,
             fetchLimit: Int? = nil,
             completion: @escaping (Result<[EntityType]>) -> Void){
        get(with: predicate,
            sortDescriptors: sortDescriptors,
            fetchLimit: fetchLimit,
            completion: completion)
    }
}

enum CoreDataWorkerError: Error{
    case cannotFetch(String)
    case cannotSave(Error)
}

class CoreDataWorker<ManagedEntity, Entity>: CoreDataWorkerProtocol where
    ManagedEntity: NSManagedObject,
    ManagedEntity: ManagedObjectProtocol,
Entity: ManagedObjectConvertible {
    
    let coreData: CoreDataServiceProtocol
    init(coreData: CoreDataServiceProtocol = CoreDataService.shared) {
        self.coreData = coreData
    }
    
    func get(with predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?, fetchLimit: Int?,
             completion: @escaping (Result<[Entity]>) -> Void) {
        coreData.performBackgroundTask { (context) in
            do {
                let fetchRequest = ManagedEntity.fetchRequest()
                fetchRequest.predicate = predicate
                fetchRequest.sortDescriptors = sortDescriptors
                fetchRequest.returnsObjectsAsFaults = false
                if let fetchLimit = fetchLimit {
                    fetchRequest.fetchLimit = fetchLimit
                }
                let results = try context.fetch(fetchRequest) as? [ManagedEntity]
                let items: [Entity] = results?.compactMap {
                    $0.toEntity() as? Entity } ?? []
                completion(.success(items))
            } catch {
                let fetchError = CoreDataWorkerError.cannotFetch("Cannot fetch error: \(error))")
                completion(.failure(fetchError))
            }
        }
    }
    
    func upsert(entity: Entity, completion: @escaping (Error?) -> Void) {
        coreData.performForegroundTask { (context) in
            
            _ = entity.toManagedObject(in: context)

            do {
                try context.save()
                completion(nil)
            } catch {
                completion(CoreDataWorkerError.cannotSave(error))
            }
        }
    }
}

