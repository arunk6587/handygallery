//
//  ManagedObjectConvertible.swift
//  DBTest
//
//  Created by Arun on 15/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//


import CoreData

protocol ManagedObjectConvertible {
    associatedtype ManagedObject: NSManagedObject
    func toManagedObject(in context: NSManagedObjectContext) -> ManagedObject?
}


extension Image: ManagedObjectProtocol {
    func toEntity() -> PictureModel? {
        return PictureModel(pictureName: name!, createdDate: createdDate!, url:URL(fileURLWithPath: "dd"), synced: isSynced)
    }
}
