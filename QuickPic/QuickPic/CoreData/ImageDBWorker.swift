//
//  ImageDBWorker.swift
//  DBTest
//
//  Created by Arun on 15/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

protocol ImageDataControllerProtocol {
    func addImages(withImage folder: PictureModel, completion: @escaping (Error?) -> Void)
    func readImagesFromDB(completion: @escaping ([PictureModel]?) -> Void)
}


class ImageDBWorker : ImageDataControllerProtocol{
    
    let worker: CoreDataWorker<Image, PictureModel>
    
    init(worker: CoreDataWorker<Image, PictureModel> = CoreDataWorker<Image, PictureModel>()){
        self.worker = worker
    }
    
    func readImagesFromDB(completion: @escaping ([PictureModel]?) -> Void)
    {
        print("\(Date()) 🗄 \(type(of: self)) \(#function) ")
        self.worker.get { (result) in
            switch result {
            case .success(let images):
                completion(images)
            case .failure(let error):
                print("\(Date()) \(error)")
                completion(nil)
            }
        }
    }
    
    func addImages(withImage image: PictureModel, completion: @escaping (Error?) -> Void)
    {
        print("\(Date()) 🗄 \(type(of: self)) \(#function) ")
        self.worker.upsert(entity: image) { (error) in
            completion(error)
        }
    }
    
    func getPictureModel(forImage: String, completion: @escaping (PictureModel?) -> Void){
        print("\(Date()) 🗄 \(type(of: self)) \(#function) [\(forImage)]")
        self.worker.get(with: NSPredicate(format: "\(DB_PIC_ID_KEY) == %@", forImage), sortDescriptors: nil, fetchLimit: 1) { (result) in
            switch result {
            case .success(let images):
                completion(images.first)
                
            case .failure(let error):
                print("\(Date()) 🗄 \(type(of: self)) \(#function) 🔴 [\(forImage)] \(error.localizedDescription)")
                completion(nil)
            }
        }
    }
    
    func updateSyncStatus(forImage: String, completion: @escaping (Error?) -> Void){
        print("\(Date()) 🗄 \(type(of: self)) \(#function) [\(forImage)]")
        self.worker.get(with: NSPredicate(format: "\(DB_PIC_ID_KEY) == %@", forImage), sortDescriptors: nil, fetchLimit: 1) { (result) in
            switch result {
            case .success(let images):
                let context = CoreDataService.shared.viewContext
                context.performAndWait({
                    let entityImage = images.first?.toManagedObject(in: context)
                    self.getSyncState(imageName: forImage, completion: { (state) in
                        entityImage?.isSynced = !state
                        do {
                            try context.save()
                            completion(nil)
                        } catch {
                            completion(CoreDataWorkerError.cannotSave(error))
                        }
                    })
                })
        
            case .failure(let error):
                print("\(Date()) 🗄 \(type(of: self)) \(#function) 🔴 [\(forImage)] \(error.localizedDescription)")
                completion(error)
            }
        }
    }
    
    
    func getSyncState(imageName: String, completion: @escaping (Bool) -> Void){
        print("\(Date()) 🗄 \(type(of: self)) \(#function) [\(imageName)]")

        self.worker.get(with: NSPredicate(format: "\(DB_PIC_ID_KEY) == %@", imageName), sortDescriptors: nil, fetchLimit: 1) { (result) in
            switch result {
            case .success(let images):
                completion((images.first?.isSynced)!)
            case .failure(let error):
                print("\(Date()) 🗄 \(type(of: self)) \(#function) 🔴 [\(imageName)] \(error.localizedDescription)")
                completion(false)
            }
        }
    }
    
    func getComment(imageName: String, completion: @escaping (String?) -> Void){
        print("\(Date()) 🗄 \(type(of: self)) \(#function) [\(imageName)]")
        
        self.worker.get(with: NSPredicate(format: "\(DB_PIC_ID_KEY) == %@", imageName), sortDescriptors: nil, fetchLimit: 1) { (result) in
            switch result {
            case .success(let images):
                let context = CoreDataService.shared.viewContext
                let image = images.first?.toManagedObject(in: context)
                if((image?.comment) != nil){
                    completion(image?.comment)
                }
                else{
                    completion("")
                }
            case .failure(let error):
                print("\(Date()) 🗄 \(type(of: self)) \(#function) 🔴 [\(imageName)] \(error.localizedDescription)")
                completion(nil)
            }
        }
    }
    
    func updateComment(forImage: String, comment:String, completion: @escaping (Error?) -> Void){
        print("\(Date()) 🗄 \(type(of: self)) \(#function) [\(forImage)]")
        self.worker.get(with: NSPredicate(format: "\(DB_PIC_ID_KEY) == %@", forImage), sortDescriptors: nil, fetchLimit: 1) { (result) in
            switch result {
            case .success(let images):
                let context = CoreDataService.shared.viewContext
                context.performAndWait({
                    let entityImage = images.first?.toManagedObject(in: context)
                    entityImage?.comment = comment
                    do {
                        try context.save()
                        completion(nil)
                    } catch {
                        completion(CoreDataWorkerError.cannotSave(error))
                    }
                })
                
            case .failure(let error):
                print("\(Date()) 🗄 \(type(of: self)) \(#function) 🔴 [\(forImage)] \(error.localizedDescription)")
                completion(error)
            }
        }
    }
}
