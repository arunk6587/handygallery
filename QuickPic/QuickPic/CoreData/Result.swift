//
//  Result.swift
//  DBTest
//
//  Created by Arun on 15/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import Foundation
enum Result<T>{
    case success(T)
    case failure(Error)
}

