//
//  BasePresenter.swift
//  QuickPic
//
//  Created by Arun on 10/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit



class BasePresenter: ViewPresenter {
    
    func userSessionValid() {
        
    }
    
    func userSessionInvalid() {
        
    }
    
    func newFileUploaded(image: String, index: Int) {
        
    }
    
    func newFileDownloaded(image: PictureModel) {
        
    }
}
