//
//  FolderListPresenter.swift
//  QuickPic
//
//  Created by Arun on 10/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import Foundation

protocol FolderListView: NSObjectProtocol {
    func foldersUpdated(folders: [FolderModel]?)
}

class FolderListPresenter: BasePresenter {

    weak private var attachedView : FolderListView?
    private var directoryService:FolderManager = FolderManager.sharedInstance

    private weak var selectedFolder:FolderModel?

    //MARK:- Init
    init(currentFolder:FolderModel){
        self.selectedFolder = currentFolder
    }
    
    //MARK:- View Actions
    func attachView(view: FolderListView){
        self.attachedView = view
    }
    
    func readFolders(){
        let (folders) = (self.directoryService.readAllFoldersInDocumentDirectory())
        if(folders != nil){
            let newFilteredList = folders?.filter {
                $0.folderName != self.selectedFolder?.folderName
            }
            self.attachedView?.foldersUpdated(folders: newFilteredList)
        }
    }
}
