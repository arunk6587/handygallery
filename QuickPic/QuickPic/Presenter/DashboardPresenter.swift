//
//  QuickPicPresenter.swift
//  QuickPic
//
//  Created by Arun on 9/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

protocol DashboardView: NSObjectProtocol {
    func directoryDetailsRefreshed(folderDetails : [BaseModel]?)
    func newItemAdded(details: BaseModel)
    func dragAndDropUpdate(details: [BaseModel], movedItems:[Int], picturesMovedToIndex:Int)
    func reloadIndex(index: Int)
    func loginUI(showLoginUI: Bool)
    func showCommentView(name: String)
}

class DashboardPresenter: BasePresenter {

    private var directoryService:FolderManager = FolderManager.sharedInstance

    weak private var attachedView : DashboardView?

    //MARK:- View Actions
    func attachView(view: DashboardView){
        self.attachedView = view
        self.directoryService.setViewCallback(sendTo: self)
    }
    
    func saveNewImage(image:UIImage){
       self.directoryService.saveCapturedImageToDocumentsDirectory(image: image, completion: { (picMode, status) in
            if(status == true){
                DispatchQueue.main.async {
                    self.attachedView?.newItemAdded(details: FolderManager.sharedInstance.getFileSystemList()!.first!)
                }
            }
        })
    }
    
    func sortImagesAfterDragNDrop(sourceImagesIndexes:[Int], toDestinationFolderIndex:Int){
        
        let (fileList, destinationFolderIndex) = directoryService.sortImagesAfterDragNDrop(sourceImagesIndexes: sourceImagesIndexes, toDestinationFolderIndex: toDestinationFolderIndex)
        DispatchQueue.main.async {

         self.attachedView?.dragAndDropUpdate(details:fileList!, movedItems: sourceImagesIndexes, picturesMovedToIndex: destinationFolderIndex)
        }
    }
    

    func addNewFolder(name:String) -> Bool{
        
        let(folder, status) = (self.directoryService.createFolderInDocmentsDirectory(withFolderName: name))
        if(status == true){
            DispatchQueue.main.async {

            self.attachedView?.newItemAdded(details: folder!)
            }
        }
        return status
    }
    
    func loginUIAction(){
        DropboxUser.sharedInstance.startNewSession()
    }
    
    func loginOutAction(){
        DropboxUser.sharedInstance.dropSession()
    }
    
    func commentButtonTapped(fileName: String){
        
        self.attachedView?.showCommentView(name: fileName)
    }
    
    func loadAllFilesAndFolders() {
        
        self.directoryService.loadDocumentsDirectoryContents(completion: { (array) in
            DispatchQueue.main.async {
                if(array != nil){
                self.attachedView?.directoryDetailsRefreshed(folderDetails:FolderManager.sharedInstance.getFileSystemList())
                }
                else{
                    self.attachedView?.directoryDetailsRefreshed(folderDetails:nil)
                }
            }
        })
    }
    
    //MARK: Dropbox Updates
    override func newFileDownloaded(image: PictureModel) {
        DispatchQueue.main.async {
            self.attachedView?.newItemAdded(details: image)
        }
    }
    
    override func newFileUploaded(image: String, index:Int) {
        DispatchQueue.main.async {
            self.attachedView?.reloadIndex(index: index)
        }
    }
    
    override func userSessionValid() {
        DispatchQueue.main.async {
            self.attachedView?.loginUI(showLoginUI: false)
        }
    }
    
    override func userSessionInvalid() {
        DispatchQueue.main.async {
            self.attachedView?.loginUI(showLoginUI: true)
        }
    }
    
}
