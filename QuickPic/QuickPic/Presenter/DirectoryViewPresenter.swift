//
//  DirectoryViewPresenter.swift
//  QuickPic
//
//  Created by Arun on 9/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import Foundation

protocol DirectoryView: NSObjectProtocol {
    func directoryDetailsRefreshed(folderDetails : [PictureModel]?)
    func updateSelectionOnUserTap(index:Int, isAdded:Bool, canEnableMove:Bool)
    func updateRearrangeMode(onReArrangeMode:Bool, refreshIndexes:[Int])
    func picturesRemoved(atIndexes: [Int], newModeList: [PictureModel])
    func showCommentView(name: String)

}

class DirectoryViewPresenter: BasePresenter {

    private var selectedIndexes:[Int] = [Int]()
    private var onReArrangeMode:Bool = false
    private var folderInAction:FolderModel? = nil
    private var directoryService:FolderManager = FolderManager.sharedInstance
    private var imagesInFolder : [PictureModel] = [PictureModel]()
    weak private var attachedView : DirectoryView?
    
    //MARK:- Init
    init(folderInAction:FolderModel){
        self.folderInAction = folderInAction
    }
    
    //MARK:- View Actions
    func attachView(view: DirectoryView){
        self.attachedView = view
    }
    
    func loadFolderContents(){
        self.folderInAction?.readPicturesFromFolder(directoryService: self.directoryService, completion: { (pictures, status) in
            DispatchQueue.main.async {
                if(status){
                    if (pictures != nil){
                        self.imagesInFolder.removeAll()
                        self.imagesInFolder.append(contentsOf: pictures!)
                        self.attachedView?.directoryDetailsRefreshed(folderDetails: pictures)
                    }
                    else{
                        self.attachedView?.directoryDetailsRefreshed(folderDetails: nil)
                    }
                }
                else{
                    self.attachedView?.directoryDetailsRefreshed(folderDetails: nil)
                }
            }
        })
    }
    
    func queryRearrangeModeState() -> Bool{
        return self.onReArrangeMode
    }
    
    
    func getSelectionStatusOfImage(atIndex index:Int) -> Bool{
        var status = false
        if(self.onReArrangeMode && self.selectedIndexes.contains(index)){
            status = true
        }
        return status
    }
    
    func switchReArrangeMode(){
        onReArrangeMode = !onReArrangeMode
        self.attachedView?.updateRearrangeMode(onReArrangeMode: onReArrangeMode, refreshIndexes:self.selectedIndexes)
        self.selectedIndexes.removeAll()
    }
    
    func moveToFolder(folder: FolderModel){
        var indexError : [Int] = [Int]()
        for picIndex in self.selectedIndexes{
            
            if(!(self.folderInAction?.cutImageToNewFolder(folder: folder, image: self.imagesInFolder[picIndex], directoryService: self.directoryService))!)
            {
                indexError.append(picIndex)
            }
        }
        if(indexError.count == 0){
            self.imagesInFolder.remove(at: self.selectedIndexes)
            self.onReArrangeMode = false
            self.attachedView?.picturesRemoved(atIndexes: self.selectedIndexes, newModeList: self.imagesInFolder)
            self.selectedIndexes.removeAll()
            self.attachedView?.updateRearrangeMode(onReArrangeMode: self.onReArrangeMode, refreshIndexes: self.selectedIndexes)
        }
    }
    
    func updateSelectionOnUserTap(pictureIndex: Int){
        if(!self.selectedIndexes.contains(pictureIndex)){
            self.selectedIndexes.append(pictureIndex)
            self.attachedView?.updateSelectionOnUserTap(index: pictureIndex, isAdded: true, canEnableMove: true)
        }
        else{
            self.selectedIndexes.remove(at: self.selectedIndexes.index(of: pictureIndex)!)
            self.attachedView?.updateSelectionOnUserTap(index: pictureIndex, isAdded: false, canEnableMove: (self.selectedIndexes.count > 0))
        }
    }
    
    func commentButtonTapped(fileName: String){
        self.attachedView?.showCommentView(name: fileName)
    }
}



