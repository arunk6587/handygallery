//
//  CommentViewerPresenter.swift
//  QuickPic
//
//  Created by Arun on 16/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

protocol CommentView: NSObjectProtocol {
    
    func commentReceived(string: String)
    func commentUpdated(string:String)
    func commentUpdateFailed()
    func imageURL(url:URL)

}


class CommentViewerPresenter: BasePresenter {

    private var directoryService:FolderManager = FolderManager.sharedInstance
    
    private var pictureObject : PictureModel? = nil

    var pictureName : String? = nil

    
    weak private var attachedView : CommentView?
    
    //MARK:- View Actions
    func attachView(view: CommentView){
        self.attachedView = view
    }
    
    func getCommentForImage(){
        self.directoryService.getPictureModel(forImage: self.pictureName!) { (picture) in
            if((picture) != nil){
                self.pictureObject = picture
                picture?.getcomment(completion: { (comment) in
                    DispatchQueue.main.async {
                        self.attachedView?.commentReceived(string: comment!)
                    }
                })
            }
            else{
                DispatchQueue.main.async {
                    self.attachedView?.commentReceived(string: "")
                }

            }
        }
    }
    
    func updateCommentforImage(withComment comment:String){
        if((pictureObject) != nil){
            pictureObject?.updateComment(image: self.pictureName!, comment: comment, completion: { (error) in
                DispatchQueue.main.async {
                    if error != nil{
                        self.attachedView?.commentUpdateFailed()
                    }
                    else{
                        self.attachedView?.commentUpdated(string: comment)
                    }
                }
            })
        }
    }
    
    func getPictureURL(){
        self.directoryService.getPictureURL(imageName: self.pictureName!) { (status, url) in
            DispatchQueue.main.async {
                if(status){
                    self.attachedView?.imageURL(url: url!)
                }
            }
        }
    }
}
