//
//  DBHandler.swift
//  QuickPic
//
//  Created by Arun on 15/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

class DBHandler: NSObject {

    private var imageWorker = ImageDBWorker()

    func addImage(image: PictureModel, completion: @escaping (Error?) -> Void){
        
        print("\(Date()) 🗄 \(type(of: self)) \(#function) Adding a new Image : \(image.pictureName ?? "0️⃣") \(image.createdDate?.description ?? "0️⃣") \(image.fileURL?.path ?? "0️⃣")")
        imageWorker.addImages(withImage: image, completion: completion)
    }

    func getSyncStateOfImage(image:String, completion: @escaping (Bool) -> Void){
        imageWorker.getSyncState(imageName: image, completion: completion)
    }
    
    func getComment(image:String, completion: @escaping (String?) -> Void){
        imageWorker.getComment(imageName: image, completion: completion)
    }
    
    func getPictureModel(forImage: String, completion: @escaping (PictureModel?) -> Void){
        imageWorker.getPictureModel(forImage: forImage, completion: completion)
    }
    
    func updateComment(image:String, comment:String, completion: @escaping (Error?) -> Void){
        imageWorker.updateComment(forImage: image, comment: comment, completion: completion)
    }
    
    func switchSyncStateOfImage(image:String, completion: @escaping (Error?) -> Void){
        print("\(Date()) 🗄 \(type(of: self)) \(#function) Sync state change for \(image)")
        imageWorker.updateSyncStatus(forImage: image, completion: completion)
    }
    
    func readImagesFromDB(completion: @escaping ([PictureModel]?) -> Void){
        
        print("\(Date()) 🗄 \(type(of: self)) \(#function) Reading all images From DB")
        imageWorker.readImagesFromDB { (images) in
            completion(images)
        }
    }
}
