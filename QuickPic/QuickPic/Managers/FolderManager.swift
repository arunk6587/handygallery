//
//  FolderManager.swift
//  QuickPic
//
//  Created by Arun on 9/6/18.
//  Copyright © 2018 Arun. All rights reserved.
//

import UIKit

protocol ViewPresenter{
    func newFileDownloaded(image:PictureModel)
    func newFileUploaded(image:String, index:Int)
    func userSessionValid()
    func userSessionInvalid()
}

protocol SyncProtocol{
    func newFileDownloaded(name: String, data:Data)
    func newFileUploaded(name: String)
}


class FolderManager {

    static let sharedInstance = FolderManager()
    
    var dbHandler : DBHandler? = DBHandler()
    
    private var dropBoxSync : DropBoxSyncController?
    private var presenter : ViewPresenter?
    private var rootFileList : [BaseModel] = [BaseModel]()

    init() {
        DropboxUser.sharedInstance.initializeUser(observer: self)
        self.dropBoxSync = DropBoxSyncController(withDBService: dbHandler!, syncProtocol: self)
    }
    
    //MARK:- Public Methods
    func setViewCallback(sendTo: ViewPresenter){
        self.presenter = sendTo
    }
    
    /**
     * Retreive all the folders amd images in documents directory
     *
     * @return [BaseModel] Array of the folders and image files sorted based on the creation date
     */
    func loadDocumentsDirectoryContents(completion: @escaping ([BaseModel]?) -> Void){

        print("\(Date()) 📂 \(type(of: self)) \(#function) ")
        DispatchQueue.global(qos: .background).async {

            let dataPath: URL = URL(fileURLWithPath: FolderManager.sharedInstance.documentDirectoryPath())
            print(dataPath)
            do{
                let resourceKeys : [URLResourceKey] = [.creationDateKey, .isDirectoryKey]
                
                let enumerator = FileManager.default.enumerator(at: dataPath, includingPropertiesForKeys: resourceKeys, options: [.skipsSubdirectoryDescendants, .skipsHiddenFiles]) { (urls, error) -> Bool in
                    return true
                }
                
                var basemodel = [BaseModel]()
                for case let fileURL as URL in enumerator! {
                    let resourceValues = try fileURL.resourceValues(forKeys: Set(resourceKeys))
                    if(resourceValues.isDirectory)!{
                        let files = try FileManager.default.contentsOfDirectory(at: fileURL, includingPropertiesForKeys: resourceKeys, options: [.skipsSubdirectoryDescendants, .skipsHiddenFiles])
                        let stringArray = files.filter { $0.pathExtension.lowercased() == IMAGE_EXTENSION_SUPPORTED}
                        let folder = FolderModel(foldername: fileURL.lastPathComponent, createdDate: resourceValues.creationDate!, picturesCount: stringArray.count, fileURL: fileURL)
                        basemodel.append(folder)
                    }
                    else{
                        let pic = PictureModel(pictureName: fileURL.lastPathComponent, createdDate: resourceValues.creationDate!, url: fileURL, synced:false)
                        pic.getSyncState()
                        basemodel.append(pic)
                    }
                }
                
                basemodel.sort {
                    $0.createdDate! > $1.createdDate!
                }
                self.rootFileList.removeAll()
                self.rootFileList.append(contentsOf: basemodel)
                DispatchQueue.main.async {
                    completion(basemodel)
                }
            }
            catch {
                print("\(Date()) 📂 🔴 \(type(of: self)) \(#function) Error while enumerating files \(dataPath.path): \(error.localizedDescription)")
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
    
    /**
     * Move an image from first level folder directory to another first level folder directory
     *
     * @param URL file url of the image to be moved
     * @param URL folder url of the destination
     * @return 2. Bool Status to indicate the file move success
     */
    func moveImage(fromPath sourceImageURL: URL, toFolder destinationFolderpath: String) -> Bool{
        
        print("\(Date()) 📂 \(type(of: self)) \(#function) [\(sourceImageURL.absoluteString)] -> [\(destinationFolderpath)]")
        let destinationPath = URL(fileURLWithPath: self.documentDirectoryPath()).appendingPathComponent("\(destinationFolderpath)")

        if (FileManager.default.fileExists(atPath: sourceImageURL.path) && FileManager.default.fileExists(atPath: destinationPath.path)){
            do {
                var destinationPath = destinationPath
                destinationPath.appendPathComponent(sourceImageURL.lastPathComponent)

                try FileManager.default.moveItem(atPath: sourceImageURL.path, toPath: destinationPath.path)
        
                return (true)
            } catch {
                print("\(Date()) 📂 🔴 \(type(of: self)) \(#function) \(error.localizedDescription)")
                
                return (false)
            }
        }
        
        return (false)
    }
    

    
    /**
     * create Folder In Docments Directory
     *
     * @param String Folder name to be created in the documents directory
     *
     * @return 1. FolderModel model object representing the newly created the folder
     * @return 2. Bool Status to indicate create success
     */
    
    func createFolderInDocmentsDirectory(withFolderName name: String) -> (FolderModel?, Bool) {
        
        print("\(Date()) 📂 \(type(of: self)) \(#function) [\(name)]")
        let filePath = URL(fileURLWithPath: self.documentDirectoryPath()).appendingPathComponent("\(name)")
        if !FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
                
                let folder = FolderModel(foldername:name, createdDate: Date(), picturesCount:0, fileURL: filePath)
                self.rootFileList.insert(folder, at: 0)
                return (folder, true)
            } catch {
                print("\(Date()) 📂 🔴 \(type(of: self)) \(#function) \(error.localizedDescription)")

                return (nil, false)
            }
        }
        
        return (nil, false)
    }
    
    func saveDownloadedImageToDocumentsDirectory(image:UIImage, name:String) -> PictureModel?{
        
        print("\(Date()) 📂 \(type(of: self)) \(#function) [\(name)]")

        let dataPath: URL = URL(fileURLWithPath: self.documentDirectoryPath()).appendingPathComponent("\(name)")
        do {
            try UIImagePNGRepresentation(image)?.write(to:dataPath, options: .atomic)
            let imageDetail = PictureModel(pictureName: name, createdDate: Date(), url: dataPath, synced: true)
            self.rootFileList.insert(imageDetail, at: 0)
            return imageDetail

        } catch {
            return nil
        }
    }
    
    /**
     * Saving the images to docuements directory.
     *
     * @param UIImage image to be copied to docuements directory.
     * @param toDestinationFolderIndex index of folder in the model
     *
     * @return 1. PictureModel model object representing the newly saved picture object
     * @return 2. Bool Status to indicate wite success
     */
    
    func saveCapturedImageToDocumentsDirectory(image:UIImage, completion: ((PictureModel?, Bool) -> Void)?){
        
        print("\(Date()) 📂 \(type(of: self)) \(#function)")
        DispatchQueue.global(qos: .background).async {
            let date = Date()
            let newFileName = date.toString(withFormat:IMAGE_NAME_FORMAT) + "." + IMAGE_EXTENSION_SUPPORTED
            let dataPath: URL = URL(fileURLWithPath: self.documentDirectoryPath()).appendingPathComponent("\(newFileName)")
            do {
                try UIImagePNGRepresentation(image)?.write(to:dataPath, options: .atomic)
                print("\(Date()) 📂  \(type(of: self)) \(#function) Write success")
                let imageDetail = PictureModel(pictureName: newFileName, createdDate: date, url: dataPath, synced: false)
                imageDetail.addToDB { (error) in
                    if(error == nil){
                        print("\(Date()) 📂  \(type(of: self)) \(#function) Save DB success")
                        self.dropBoxSync?.newFileToUpload(file: newFileName)
                        self.rootFileList.insert(imageDetail, at: 0)
                        completion!(imageDetail, true)
                    }
                    else{
                        print("\(Date()) 📂 🔴 \(type(of: self)) \(#function) Failed to add to foolder")
                        completion!(nil, false)

                    }
                }
            }
            catch {
                print("\(Date()) 📂 🔴 \(type(of: self)) \(#function) Save failed")
                completion!(nil, false)
            }
        }
    }
    
    /**
     * Sorting and rearraging the model after moving the images to appropriate directory based on the Model array indexes
     *
     * @param sourceImagesIndexes array of indexes from where the images are moved from
     * @param toDestinationFolderIndex index of folder in the model
     *
     * @return 1. Updated list of folder and images model..
     * @return 2. Bool Status to indicate merging is success
     */
    
    func sortImagesAfterDragNDrop(sourceImagesIndexes:[Int], toDestinationFolderIndex:Int) -> ([BaseModel]?, Int){
        print("\(Date()) 📂 \(type(of: self)) \(#function)")
        var counter : Int = 0
        let folderDetail = self.rootFileList[toDestinationFolderIndex] as! FolderModel
        let sourceIndexpathArray = sourceImagesIndexes.sorted{
            ($0) < ($1)
        }
        
        var destinationOffsetShifted = 0
        
        for sourceitem in sourceIndexpathArray{
            var offsetIndex = sourceitem
            var picDetail : PictureModel? = nil
            if(sourceitem < toDestinationFolderIndex){
                offsetIndex = sourceitem - counter
                destinationOffsetShifted += 1
            }
            else{
                offsetIndex -= counter
            }
            picDetail = self.rootFileList[offsetIndex] as? PictureModel
            
            let isSucess = folderDetail.moveImage(image: picDetail!, directoryService: self)
            if(isSucess){
                self.rootFileList.remove(at: offsetIndex)
                counter += 1
            }
        }
        
        destinationOffsetShifted = toDestinationFolderIndex - destinationOffsetShifted
        
        print("\(Date()) 📂 \(type(of: self)) \(#function) Succeeded")

        return (self.getFileSystemList(), destinationOffsetShifted)
    }
    

    /**
     * Read the list of pictures from a folder
     *
     * @param String First level folder name in the documents directory
     *
     * @return 1. Bool Status to indicate read is success
     * @return 2. The Picture objects with thumbnail from the folder.
     */
    func readPicturesFromFolder(folderName name: String, completion: @escaping (([PictureModel]?, Bool) -> Void))
    {
        DispatchQueue.global(qos: .background).async {
            let folderPath: URL = URL(fileURLWithPath: self.documentDirectoryPath()).appendingPathComponent("\(name)")

            print("\(Date()) 📂 \(type(of: self)) \(#function) [\(name)]")

            if FileManager.default.fileExists(atPath: folderPath.path) {
                do{
                    let resourceKeys : [URLResourceKey] = [.creationDateKey, .isDirectoryKey]
                    
                    let enumerator = FileManager.default.enumerator(at: folderPath, includingPropertiesForKeys: resourceKeys, options: [.skipsSubdirectoryDescendants, .skipsHiddenFiles]) { (urls, error) -> Bool in
                        return true
                    }
                    
                    var basemodel = [PictureModel]()
                    for case let fileURL as URL in enumerator! {
                        let resourceValues = try fileURL.resourceValues(forKeys: Set(resourceKeys))
                        if(!resourceValues.isDirectory! && fileURL.pathExtension.lowercased() == IMAGE_EXTENSION_SUPPORTED){
                            let pic = PictureModel(pictureName: fileURL.lastPathComponent, createdDate: resourceValues.creationDate!, url: fileURL, synced: false)
                            pic.getSyncState()
                            print("\(Date()) 📂 \(type(of: self)) \(#function) File read [\(fileURL.lastPathComponent)]")
                            basemodel.append(pic)
                        }
                    }
                    
                    basemodel.sort {
                        $0.createdDate! > $1.createdDate!
                    }
                    
                    print("\(Date()) 📂 \(type(of: self)) \(#function) Files read")
                    
                    completion(basemodel, true)
                }
                catch {
                    
                    print("\(Date()) 📂 🔴 \(type(of: self)) \(#function) Error while enumerating files \(folderPath.path): \(error.localizedDescription)")
                    completion(nil, false)
                }
            }
            else{
                completion(nil, false)
            }
        }
    }
    
    func getPictureURL(imageName name: String, completion: @escaping ((Bool, URL?) -> Void)){
        
        print("\(Date()) 📂 \(type(of: self)) \(#function) [\(name)]")

        DispatchQueue.global(qos: .background).async {
            let folderPath: URL = URL(fileURLWithPath: self.documentDirectoryPath())
            if FileManager.default.fileExists(atPath: folderPath.path) {
                do{
                    let resourceKeys : [URLResourceKey] = [.creationDateKey, .isDirectoryKey]
                    
                    let enumerator = FileManager.default.enumerator(at: folderPath, includingPropertiesForKeys: resourceKeys, options: [.skipsHiddenFiles]) { (urls, error) -> Bool in
                        return true
                    }
                    var imageURL : URL? = nil
                    var status = false
                    for case let fileURL as URL in enumerator! {
                        let resourceValues = try fileURL.resourceValues(forKeys: Set(resourceKeys))
                        if(!resourceValues.isDirectory! && fileURL.lastPathComponent == name && fileURL.pathExtension.lowercased() == IMAGE_EXTENSION_SUPPORTED){
                            imageURL = fileURL
                            status = true
                            break
                        }
                    }
                    print("\(Date()) 📂 \(type(of: self)) \(#function) [\(imageURL?.absoluteString ?? "🔴 Not found")]")
                    completion(status, imageURL)
                }
                catch {
                    print("\(Date()) 📂 🔴 \(type(of: self)) \(#function) Error while enumerating files \(folderPath.path): \(error.localizedDescription)")
                    completion (false, nil)
                }
            }
            else{
                completion (false, nil)
            }
        }
    }
    /**
     * Read the folders from the documents directory
     * @return List of folders
     */
    func readAllFoldersInDocumentDirectory() -> ([FolderModel]?){
        
        let folderArray = self.rootFileList.filter { $0.modelType == .Folder_Type}
        return (folderArray as? [FolderModel])
    }
 
    /**
     * Getter for the filelist array object
     * @return Sorted List of folders and images
     */
    func getFileSystemList() -> [BaseModel]?{
        return self.rootFileList
    }
    
    func getPictureModel(forImage: String, completion: @escaping (PictureModel?) -> Void){
        self.dbHandler?.getPictureModel(forImage: forImage, completion: completion)
    }
    
    private func documentDirectoryPath() -> String{
        
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
}

//MARK:- SyncProtocol
extension FolderManager: SyncProtocol{
    
    func newFileDownloaded(name: String, data: Data) {
        print("\(Date()) 🛎 📂\(type(of: self)) \(#function) [\(name)]")
        let picModel = self.saveDownloadedImageToDocumentsDirectory(image: UIImage(data: data)!, name: name)
            if let picModel = picModel{
                picModel.addToDB { (error) in
                    if(error == nil){
                        DispatchQueue.main.async {
                            self.presenter?.newFileDownloaded(image: picModel)
                        }
                    }
                }
        }
    }
    
    func newFileUploaded(name: String) {
        print("\(Date()) 🛎 📂 \(type(of: self)) \(#function) [\(name)]")
        let fileList = self.rootFileList.filter { (model) -> Bool in
            (model.modelType == .Picture_Type && (model as! PictureModel).pictureName == name)
        }
        if let file = fileList.first{
            var index = 0
            index = self.rootFileList.index(of: file)!
            (file as! PictureModel).switchSyncState { (error) in
                if error == nil{
                    DispatchQueue.main.async {
                        self.presenter?.newFileUploaded(image: name, index: index)
                    }
                }
            }
        }
        else{
            let picModel : PictureModel = PictureModel.getDummyObject(name: name)
            picModel.switchSyncState { (error) in
                if error == nil{
                    DispatchQueue.main.async {
                        self.presenter?.newFileUploaded(image: name, index: 0)
                    }
                }
            }
        }
        print("\(Date()) 📂 \(type(of: self)) \(#function) Uploaded [\(name)]")
    }

}

//MARK:- DropboxUserSession
extension FolderManager : DropboxUserSession{

    func userSessionValid(shouldStartSync: Bool) {
        print("\(Date()) 🛎 📂 \(type(of: self)) \(#function) >> 📦 Preparing for Sync \(shouldStartSync ? "Yes" : "No")")
        self.presenter?.userSessionValid()
        if(shouldStartSync){
            self.dropBoxSync?.prepareForSync()
        }
    }
    
    func sessionInvalid() {
        print("\(Date()) 🛎 📂 \(type(of: self)) \(#function) >> 📦 Cancel Sync")
        self.presenter?.userSessionInvalid()
        self.dropBoxSync?.cancelOperations()
    }
    
    func userAuthError(message: String) {
        print("\(Date()) 🛎 📂 \(type(of: self)) \(#function) >> 📦 Cancel Sync")
        self.presenter?.userSessionInvalid()
        self.dropBoxSync?.cancelOperations()
    }
}
